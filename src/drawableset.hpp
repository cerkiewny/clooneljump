/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef idBFBF4D5B34DD49E68B89249AA1D8FCE4
#define idBFBF4D5B34DD49E68B89249AA1D8FCE4

#include <vector>

namespace cloonel {
	class Drawable;

	class DrawableSet {
	public:
		virtual void CopyDrawables ( std::vector<const Drawable*>& parOut ) const = 0;
	};
} //namespace cloonel

#endif
