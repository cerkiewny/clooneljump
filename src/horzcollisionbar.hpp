/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id202E39A912AA43E5A224AC77D676F6CA
#define id202E39A912AA43E5A224AC77D676F6CA

#include "vector.hpp"
#include "placeable.hpp"
#include "line.hpp"
#include "compatibility.h"
#include <functional>

namespace cloonel {
	class HorzCollisionBar;

	class HorzCollisionBar : public Placeable {
	public:
		typedef Line<float, 2> Line2D;
		typedef std::function<void(const Line2D&, const float2&)> CallbackType;

	public:
		HorzCollisionBar ( const float2& parFrom, float parLength );
		virtual ~HorzCollisionBar ( void ) noexcept = default;

		float2 From ( void ) const { return this->GetPos(); }
		float2 To ( void ) const;

		void SetCallback ( CallbackType parCallback );
		void InvokeCallback ( const Line2D& parOverlap, const float2& parDirection ) const;
		const float2& GetOffset ( void ) const noexcept { return m_offset; }

		//Overrides
		virtual void AddOffset ( const float2& parOffset ) noexcept;
		virtual void BeginMovement ( void );

	private:
		friend bool Collide ( float parDeltaT, const HorzCollisionBar& parA, const HorzCollisionBar& parB, Line2D& parOut );

		Line2D m_segment;
		float2 m_offset;
		CallbackType m_callback;
	};

	bool Collide ( float parDeltaT, const HorzCollisionBar& parA, const HorzCollisionBar& parB, HorzCollisionBar::Line2D& parOut ) a_pure;
} //namespace cloonel

#endif
