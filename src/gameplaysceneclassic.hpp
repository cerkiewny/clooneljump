/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idF6FF1F57C36842DC9B20E2F55C507C2E
#define idF6FF1F57C36842DC9B20E2F55C507C2E

#include "gameplayscene.hpp"
#include <memory>

namespace cloonel {
	class SDLMain;
	class Character;
	class MoverSine;
	class MoverLeftRight;
	class MoverWorld;
	class TiledWallpaper;
	class Texture;
	class PlatformSpawner;

	class GameplaySceneClassic : public GameplayScene {
	public:
		explicit GameplaySceneClassic ( SDLMain* parSdlMain );
		virtual ~GameplaySceneClassic ( void ) noexcept;

		virtual void Destroy ( void ) noexcept;

	private:
		virtual void OnPreUpdate ( void );
		virtual void OnPrepare ( void );

		std::unique_ptr<Character> m_player;
		std::unique_ptr<MoverSine> m_moverSine;
		std::unique_ptr<MoverLeftRight> m_moverLeftRight;
		std::unique_ptr<MoverWorld> m_moverWorld;
		std::unique_ptr<TiledWallpaper> m_wallpaper;
		std::unique_ptr<PlatformSpawner> m_platforms;
	};
} //namespace cloonel

#endif
