/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id8C7FE975525B4329BFBEAF364D934EAD
#define id8C7FE975525B4329BFBEAF364D934EAD

#include <memory>

namespace cloonel {
	class SDLMain;
	class Texture;
	class InputBag;

	class GameBase {
	public:
		float Exec ( void );
		bool WantsToQuit ( void ) const;

		void Prepare ( void );
		virtual void Destroy ( void ) noexcept = 0;

	protected:
		explicit GameBase ( SDLMain* parSdlMain );
		virtual ~GameBase ( void ) noexcept;

		SDLMain* SDLObject ( void ) { return m_sdlmain; }
		InputBag* InputBagObject ( void ) { return m_input.get(); }

	private:
		virtual void OnRender ( void ) = 0;
		virtual void OnPreUpdate ( void ) = 0;
		virtual void OnUpdate ( float parDelta ) = 0;
		virtual void OnPrepare ( void ) = 0;
		virtual void OnPrepareDone ( void ) = 0;
		virtual bool ShouldQuit ( void ) const;

		const std::unique_ptr<InputBag> m_input;
		SDLMain* const m_sdlmain;
		unsigned int m_time0;
		bool m_wantsToQuit;
	};
} //namespace cloonel

#endif
