/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id8714F4436A1F462193A253C8C5AF55CD
#define id8714F4436A1F462193A253C8C5AF55CD

#include "compatibility.h"
#if !defined(NDEBUG)
#include <cassert>
#endif

namespace cloonel {
	template <typename To, typename From>
	To checked_numcast ( From parFrom ) a_pure;

	template <typename To, typename From>
	inline
	To checked_numcast (From parFrom) {
#if defined(__INTEL_COMPILER)
//implicit conversion of a 64-bit integral type to a smaller integral type (potential portability problem)
#	pragma warning push
#	pragma warning disable 1682
#endif

#if defined(NDEBUG)
		return static_cast<To>(parFrom);
#else
		const To retVal = static_cast<To>(parFrom);
		assert(static_cast<From>(retVal) == parFrom);
		return retVal;
#endif

#if defined(__INTEL_COMPILER)
#	pragma warning pop
#endif
	}
} //namespace cloonel

#endif
