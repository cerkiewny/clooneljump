/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef idE8A33294FB164199B0A23E0385DD0E09
#define idE8A33294FB164199B0A23E0385DD0E09

#if defined(WITH_DEBUG_VISUALS)

#include "colour.hpp"
#include "line.hpp"
#include <cstdint>

namespace cloonel {
	class SDLMain;

	class DrawableLine : public Line<int16_t, 2> {
		typedef Line<int16_t, 2> LineBase;
	public:
		explicit DrawableLine ( SDLMain* parMain );
		explicit DrawableLine ( SDLMain* parMain, Colour parColour );
		DrawableLine ( SDLMain* parMain, Colour parColour, const LineBase& parLine );
		DrawableLine ( SDLMain* parMain, Colour parColour, const LineBase::Point& parStart, const LineBase::Point& parEnd );
		DrawableLine ( const DrawableLine& parOther ) = default;
		virtual ~DrawableLine ( void ) noexcept = default;

		DrawableLine& operator= ( const DrawableLine& ) = delete;

		void Render ( const float2& parPos, const float2& parScaling ) const;

	private:
		SDLMain* const m_sdlmain;
		Colour m_colour;
	};

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	inline DrawableLine::DrawableLine (SDLMain* parMain) :
		m_sdlmain(parMain),
		m_colour()
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	inline DrawableLine::DrawableLine (SDLMain* parMain, Colour parColour) :
		m_sdlmain(parMain),
		m_colour(parColour)
	{
	}
} //namespace cloonel

#endif

#endif
