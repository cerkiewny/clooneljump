/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id17908979556C47F8A978688BBE4A9D22

#include "placeable.hpp"
#include "collidertypedef.hpp"
#include "collider.hpp"
#include <memory>

namespace cloonel {
	class SDLMain;
	class GameplayScene;
	class DrawableSet;

	class PlatformSpawner : public Placeable {
	public:
		PlatformSpawner ( void ) = delete;
		PlatformSpawner ( const char* parTexturePath, SDLMain* parSDLMain, GameplayScene* parScene, float parMaxDistance );
		PlatformSpawner ( const PlatformSpawner& ) = delete;
		PlatformSpawner ( PlatformSpawner&& parOther ) = delete;
		virtual ~PlatformSpawner ( void ) noexcept;
		PlatformSpawner& operator= ( const PlatformSpawner& ) = delete;

		void Prepare ( void );
		void Destroy ( void ) noexcept;
		void SpawnPlatforms ( void );
		void RegisterForCollision ( Collider& parCollider, Collider::GroupIDType parGroupID );
		const DrawableSet* GetDrawableSet ( void ) const;

	private:
		struct LocalData;

		//Overrides
		virtual void BeginMovement ( void );
		virtual void AddOffset ( const float2& parOffset ) noexcept;
		virtual void SetAbsolutePosition ( const float2& ) noexcept;

		const std::unique_ptr<LocalData> m_localdata;
		float m_targetAverage;
	};
} //namespace cloonel

#endif
