/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "platform.hpp"
#include "texture.hpp"
#include "horzcollisionbar.hpp"
#include <cassert>
#include <algorithm>

namespace cloonel {
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	Platform::Platform (SDLMain* parSdlMain, const float2& parPos, Texture* parTexture, const float2& parSize) :
		Placeable(parPos),
		m_screenRatio(parSdlMain),
		m_size(parSize),
		m_collisionTop(new HorzCollisionBar(parPos, parSize.x())),
		m_surface(parTexture)
	{
		assert(m_surface);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	Platform::Platform (Platform&& parOther) noexcept :
		Placeable(parOther.GetPos()),
		m_screenRatio(std::move(parOther.m_screenRatio)),
		m_size(parOther.m_size),
		m_collisionTop(std::move(parOther.m_collisionTop)),
		m_surface(parOther.m_surface)
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	Platform::~Platform() noexcept {
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void Platform::Draw() const {
		m_surface->Render(TopLeft(), m_size, m_screenRatio.Ratio(), true);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	Platform& Platform::operator= (const Platform& parOther) {
		Drawable::operator= (parOther);
		Placeable::operator= (parOther);

		m_size = parOther.m_size;
		m_surface = parOther.m_surface;
		return *this;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void Platform::OnRegister (Mover& parMover, Mover::PlaceableTicketType parParentTicket) {
		parMover.RegisterPlaceable(m_collisionTop.get(), parParentTicket);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void Platform::CopyDrawables (std::vector<const Drawable*>& parOut) const {
		parOut.push_back(this);
	}
} //namespace cloonel
