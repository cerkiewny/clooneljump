/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "drawableline.hpp"
#include "sdlmain.hpp"
#include "geometry_2d.hpp"
#if !defined(NDEBUG)
#	include "line_helpers.hpp"
#endif
#include <SDL2/SDL.h>
#include <algorithm>

#if defined(WITH_DEBUG_VISUALS)
namespace cloonel {
	namespace {
		///----------------------------------------------------------------------
		///----------------------------------------------------------------------
		void ClipLine (const SDL_Rect& parArea, Line<int16_t, 2>& parLine) {
			Line<float, 2> line(static_cast<float2>(parLine[0]), static_cast<float2>(parLine[1]));
			const float al = static_cast<float>(parArea.x);
			const float at = static_cast<float>(parArea.y);
			const float ar = static_cast<float>(parArea.x + parArea.w);
			const float ab = static_cast<float>(parArea.y + parArea.h);

			{
				const Line<float, 2> leftLine(al, at, al, ab);
				const auto intersection(Intersection(line, leftLine));
				if (intersection.first) {
					if (line[0] < leftLine)
						line[0] = intersection.second;
					if (line[1] < leftLine)
						line[1] = intersection.second;
				}
			}
			{
				const Line<float, 2> rightLine(ar, at, ar, ab);
				const auto intersection(Intersection(line, rightLine));
				if (intersection.first) {
					if (line[0] > rightLine)
						line[0] = intersection.second;
					if (line[1] > rightLine)
						line[1] = intersection.second;
				}
			}
			{
				const Line<float, 2> topLine(al, at, ar, at);
				const auto intersection(Intersection(line, topLine));
				if (intersection.first) {
					if (line[0] < topLine)
						line[0] = intersection.second;
					if (line[1] < topLine)
						line[1] = intersection.second;
				}
			}
			{
				const Line<float, 2> bottomLine(al, ab, ar, ab);
				const auto intersection(Intersection(line, bottomLine));
				if (intersection.first) {
					if (line[0] > bottomLine)
						line[0] = intersection.second;
					if (line[1] > bottomLine)
						line[1] = intersection.second;
				}
			}

			parLine[0] = static_cast<short2>(line[0]);
			parLine[1] = static_cast<short2>(line[1]);
		}
	} //unnamed namespace

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	DrawableLine::DrawableLine (SDLMain* parMain, Colour parColour, const LineBase& parLine) :
		LineBase(parLine),
		m_sdlmain(parMain),
		m_colour(parColour)
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	DrawableLine::DrawableLine (SDLMain* parMain, Colour parColour, const LineBase::Point& parStart, const LineBase::Point& parEnd) :
		LineBase(parStart, parEnd),
		m_sdlmain(parMain),
		m_colour(parColour)
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void DrawableLine::Render (const float2& parPos, const float2& parScaling) const {
		SDL_SetRenderDrawColor(m_sdlmain->GetRenderer(), m_colour.r, m_colour.g, m_colour.b, m_colour.a);

		LineBase scaledLine(*this);
		scaledLine += static_cast<short2>(parPos);
		scaledLine *= parScaling;
		{
			SDL_Rect screen;
			screen.x = screen.y = 0;
			const short2 wh(static_cast<short2>(m_sdlmain->WidthHeight()));
			screen.w = wh.x();
			screen.h = wh.y();

			ClipLine(screen, scaledLine);
		}
		const int16_t h = static_cast<int16_t>(m_sdlmain->WidthHeight().y());
		if (scaledLine[0] != scaledLine[1]) {
			SDL_RenderDrawLine(
				m_sdlmain->GetRenderer(),
				scaledLine[0].x(),
				h - scaledLine[0].y(),
				scaledLine[1].x(),
				h - scaledLine[1].y()
			);
		}
		//Returns 0 on success or a negative error code on failure; call SDL_GetError() for more information.
		//http://www.ginkgobitter.org/sdl/?SDL_RenderDrawLine
	}
} //namespace cloonel
#endif
