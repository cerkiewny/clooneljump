/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace cloonel {
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	T len (const Line<T, S>& parLine) {
		return std::sqrt(len_sq(parLine));
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	T len_sq (const Line<T, S>& parLine) {
		T res(0);
		const typename Line<T, S>::Point& start = parLine.Start();
		const typename Line<T, S>::Point& end = parLine.End();

		for (uint32_t z = 0; z < S; ++z) {
			const T diff(end[z] - start[z]);
			res += diff * diff;
		}
		return res;
	}
} //namespace cloonel
