/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idBAA1271995604A659EF3FEC4B8FC3870
#define idBAA1271995604A659EF3FEC4B8FC3870

//Move a Placeable by issuing an offset relative to a base position.
//Use this if for example your Placeable is following a mathematical formula or
//if you're giving a position you want it to be at at any specific time.
//For example issuing 1, then 2 will put your Placeable in position 2,
//assuming it started at 0.

#include "mover.hpp"

namespace cloonel {
	class MoverRelative : public Mover {
	public:
		MoverRelative ( void );
		virtual ~MoverRelative ( void ) noexcept = default;

		virtual void Update ( float parDelta );

	protected:
		virtual void UpdateSingle ( Placeable* parPlaceable );
		virtual float2 GetOffset ( void ) const = 0;

	private:
		float2 m_prevOffset;
	};
} //namespace cloonel

#endif
