/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id9CDCF10D483641A2845353C0835F93EC
#define id9CDCF10D483641A2845353C0835F93EC

//Move a Placeable by issuing an offset.
//The offset is applied to the current position of the Placeable you are
//moving.
//For example issuing 1, then 2 will put your Placeable in position 3,
//assuming it started at 0.

#include "vector.hpp"
#include "mover.hpp"

namespace cloonel {
	class MoverOneShot : public Mover {
	public:
		MoverOneShot ( void ) = default;
		virtual ~MoverOneShot ( void ) noexcept = default;

	private:
		virtual void UpdateSingle ( Placeable* parPlaceable );
		virtual float2 GetOffset ( void ) const = 0;
	};
} //namespace cloonel

#endif
