/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "mover.hpp"
#include "placeable.hpp"
#include <cassert>

namespace cloonel {
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	Mover::~Mover() noexcept {
		m_placeables.RemoveAll();
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void Mover::Update (float parDelta) {
		ApplyMotion(parDelta);
		for (auto currPlaceable : m_placeables) {
			UpdateSingle(currPlaceable);
		}
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	Mover::PlaceableTicketType Mover::RegisterPlaceable (Placeable* parPlaceable, PlaceableTicketType parTicket) {
		assert(parPlaceable);
		const auto currTicket = m_placeables.Add(parPlaceable, parTicket);
		parPlaceable->OnRegister(*this, currTicket);
		return currTicket;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void Mover::CopyPlaceables (std::unordered_set<Placeable*>& parOut) {
		m_placeables.CopyObservers(parOut);
	}
} //namespace cloonel
