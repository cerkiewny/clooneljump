/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "moversine.hpp"
#if !defined(_GNU_SOURCE)
#	define _GNU_SOURCE
#endif
#include <cmath>

namespace cloonel {
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	MoverSine::MoverSine() :
		MoverRelative(),
		m_alpha(0.0f),
		m_power(1.0f)
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	float2 MoverSine::GetOffset() const {
		return float2(0.0f, std::abs(std::sin(m_alpha)) * m_power);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void MoverSine::ApplyMotion (float parDelta) {
		const float pitwo = static_cast<float>(M_PI) * 2.0f;
		m_alpha += parDelta * 2.6f;
		//TODO: optimize - see https://groups.google.com/forum/#!topic/comp.graphics.api.opengl/FFl3djAYERs
		while (m_alpha >= pitwo)
			m_alpha -= pitwo;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void MoverSine::ResetToBounce() {
		m_alpha = 0.0f;
	}
} //namespace cloonel
