/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id16A9347A373E4144A9C2B98E7D7A1351
#define id16A9347A373E4144A9C2B98E7D7A1351

#include "moverrelative.hpp"

namespace cloonel {
	class MoverSine : public MoverRelative {
	public:
		MoverSine ( void );
		~MoverSine ( void ) noexcept = default;

		void SetPower ( float parPower ) noexcept { m_power = parPower; }
		void ResetToBounce ( void );

	private:
		virtual void ApplyMotion ( float parDelta );
		virtual float2 GetOffset() const;

		float m_alpha;
		float m_power;
	};
} //namespace cloonel

#endif
