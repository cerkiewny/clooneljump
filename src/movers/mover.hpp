/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id20409B43E62B4247A278B413D1114896
#define id20409B43E62B4247A278B413D1114896

#include "vector.hpp"
#include "observersmanager.hpp"
#include <unordered_set>

namespace cloonel {
	class Placeable;

	class Mover {
	public:
		typedef ObserversManager<Placeable*>::TicketType PlaceableTicketType;

		enum {
			NullTicket = ObserversManager<Placeable*>::Ticket_Null
		};

		Mover ( void ) = default;
		virtual ~Mover ( void ) noexcept;

		virtual void Update ( float parDelta );
		PlaceableTicketType RegisterPlaceable ( Placeable* parPlaceable, PlaceableTicketType parParent=ObserversManager<Placeable*>::Ticket_Null );
		void UnregisterPlaceable ( PlaceableTicketType parID ) noexcept { m_placeables.Remove(parID); }
		void CopyPlaceables ( std::unordered_set<Placeable*>& parOut );

	protected:
		std::size_t PlaceableCount ( void ) const { return m_placeables.size(); }

	private:
		virtual void ApplyMotion ( float parDelta ) = 0;
		virtual void UpdateSingle ( Placeable* parPlaceable ) = 0;
		void UpdateAll ( float parDelta );

		ObserversManager<Placeable*> m_placeables;
	};
} //namespace cloonel

#endif
