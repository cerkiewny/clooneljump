/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "platformset.hpp"
#include "platform.hpp"
#include "CloonelJumpConfig.h"
#include "casts.hpp"
#include <ciso646>
#include <algorithm>
#include <cassert>
#include <memory>
#include <functional>
#include <algorithm>

namespace cloonel {
	namespace {
		///----------------------------------------------------------------------
		///----------------------------------------------------------------------
		boost::circular_buffer<Platform>::const_iterator FindFirstVisible (const boost::circular_buffer<Platform>& parPlatforms) {
			auto curr = parPlatforms.begin();
			while (curr != parPlatforms.end() and curr->TopLeft().y() < 0.0f) {
				++curr;
			}
			return curr;
		}
	} //unnamed namespace

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	PlatformSet::PlatformSet (SDLMain* parSdl) :
		m_platforms(MAX_PLATFORMS_ON_SCREEN),
		m_sdlmain(parSdl)
	{
		assert(m_sdlmain);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	PlatformSet::~PlatformSet() noexcept {
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSet::Add (const float2& parPos, const float2& parSize, Texture* parTexture) {
		m_platforms.push_back(Platform(m_sdlmain, parPos, parTexture, parSize));
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSet::CopyBars (std::vector<const HorzCollisionBar*>& parOut) const {
		auto eleCopy = FindFirstVisible(m_platforms);
		const size_t count = checked_numcast<size_t>(m_platforms.end() - eleCopy);
		parOut.reserve(parOut.size() + count);
		while (m_platforms.end() != eleCopy) {
			parOut.push_back(eleCopy->TopCollisionBar());
			++eleCopy;
		}
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSet::CopyDrawables (std::vector<const Drawable*>& parOut) const {
		auto eleCopy = FindFirstVisible(m_platforms);
		const size_t count = checked_numcast<size_t>(m_platforms.end() - eleCopy);
		parOut.reserve(parOut.size() + count * Platform::SurfaceCount);
		while (m_platforms.end() != eleCopy) {
			eleCopy->CopyDrawables(parOut);
			++eleCopy;
		}
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	size_t PlatformSet::CountFreePlatforms() const {
		size_t freePlatforms = 0;
		for (const auto& platf : m_platforms) {
			if (platf.GetPos().y() < 0.0f)
				++freePlatforms;
			else
				break;
		}
		return MAX_PLATFORMS_ON_SCREEN - m_platforms.size() + freePlatforms;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSet::clear() noexcept {
		m_platforms.clear();
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSet::SendBeginMovement() noexcept {
		std::for_each(m_platforms.begin(), m_platforms.end(), std::bind(&Platform::BeginMovement, std::placeholders::_1));
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSet::SendAddOffset (const float2& parOffset) noexcept {
		std::for_each(m_platforms.begin(), m_platforms.end(), std::bind(&Platform::AddOffset, std::placeholders::_1, parOffset));
	}
} //namespace cloonel
