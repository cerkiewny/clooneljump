/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef id44C452F5B87A4993B127EFE654837C7D
#define id44C452F5B87A4993B127EFE654837C7D

#include "compatibility.h"

namespace cloonel {
	template <typename T, typename U>
	T Lerp ( const T& parStart, const T& parEnd, const U& parPercent ) a_pure;
	template <typename T, typename R, typename U>
	T LerpRange ( const T& parStart, const R& parRange, const U& parPercent ) a_pure;

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, typename U>
	T Lerp (const T& parStart, const T& parEnd, const U& parPercent) {
		return parStart + parPercent * (parEnd - parStart);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, typename R, typename U>
	T LerpRange (const T& parStart, const R& parRange, const U& parPercent) {
		return parStart + parPercent * parRange;
	}
} //namespace cloonel

#endif
