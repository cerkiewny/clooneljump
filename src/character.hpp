/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id0CEACFB045ED4C9F8688265AA41E30B0
#define id0CEACFB045ED4C9F8688265AA41E30B0

#include "placeable.hpp"
#include "drawable.hpp"
#include "drawableset.hpp"
#include "vector.hpp"
#include "sizenotifiable.hpp"
#include "horzcollisionbar.hpp"
#include "collidertypedef.hpp"
#if defined(WITH_DEBUG_VISUALS)
# include "drawableline.hpp"
#endif
#include <string>
#include <memory>
#include <cstdint>
#include <functional>
#include <vector>

namespace cloonel {
	class SDLMain;
	class Texture;
	template <typename T, uint32_t S> class Line;

	class Character : public Placeable, public Drawable, public DrawableSet {
	public:
		typedef std::function<void(const Line<float, 2>&, const float2&)> BounceCallbackType;

		Character ( const std::string& parPath, SDLMain* parMain, float2 parSize );
		Character ( const std::string&& parPath, SDLMain* parMain, float2 parSize );
		Character ( const Character& ) = delete;
		virtual ~Character ( void ) noexcept;

		void Prepare ( void );
		void Destroy ( void ) noexcept;
		virtual void Draw ( void ) const;
		void RegisterForCollision ( ColliderRegisterFunc parRegisterCollision );
		void SetOnBounceCallback ( BounceCallbackType parCallb );

	private:
		//Overrides
		virtual void OnRegister ( Mover& parMover, Mover::PlaceableTicketType parParentTicket );
		virtual void CopyDrawables ( std::vector<const Drawable*>& parOut ) const { parOut.push_back(this); }

		void OnBounce ( const Line<float, 2>& parCollision, const float2& parDirection );

		HorzCollisionBar m_bottomBar;
		SizeNotifiable<regbehaviours::AutoRegister> m_screenRatio;
		BounceCallbackType m_bounceCallback;
		const std::unique_ptr<Texture> m_texture;
#if defined(WITH_DEBUG_VISUALS)
		DrawableLine m_bottomBarDrawable;
#endif
	};
} //unnamed namespace

#endif
