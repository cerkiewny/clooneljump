/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "character.hpp"
#include "sdlmain.hpp"
#include "texture.hpp"
#include "collider.hpp"
#include "line.hpp"
#include <cassert>

namespace cloonel {
	namespace {
		void DoNothing (const Line<float, 2>&, const float2&) {
		}
	} //unnamed namespace

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	Character::Character (const std::string& parPath, SDLMain* parMain, float2 parSize) :
		Placeable(float2(0.0f)),
		Drawable(parSize),
		m_bottomBar(float2(0.0f), parSize.x()),
		m_screenRatio(parMain),
		m_bounceCallback(&DoNothing),
		m_texture(new Texture(parPath, parMain, false))
#if defined(WITH_DEBUG_VISUALS)
		, m_bottomBarDrawable(parMain, Colour(250, 5, 1), static_cast<short2>(m_bottomBar.From()), static_cast<short2>(m_bottomBar.To()))
#endif
	{
		assert(parMain);
		m_bottomBar.SetCallback(std::bind(&Character::OnBounce, this, std::placeholders::_1, std::placeholders::_2));
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	Character::Character (const std::string&& parPath, SDLMain* parMain, float2 parSize) :
		Placeable(float2(0.0f)),
		Drawable(parSize),
		m_bottomBar(float2(0.0f), parSize.x()),
		m_screenRatio(parMain),
		m_bounceCallback(&DoNothing),
		m_texture(new Texture(parPath, parMain, false))
#if defined(WITH_DEBUG_VISUALS)
		, m_bottomBarDrawable(parMain, Colour(250, 5, 1), static_cast<short2>(m_bottomBar.From()), static_cast<short2>(m_bottomBar.To()))
#endif
	{
		assert(parMain);
		m_bottomBar.SetCallback(std::bind(&Character::OnBounce, this, std::placeholders::_1, std::placeholders::_2));
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	Character::~Character() noexcept {
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void Character::RegisterForCollision (ColliderRegisterFunc parRegisterCollision) {
		parRegisterCollision(&m_bottomBar);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void Character::Prepare() {
		const SDLMain* const sdlmain = m_texture->SDLObject();
		const int2 screensize(sdlmain->WidthHeight());

		m_texture->Reload();
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void Character::Destroy() noexcept {
		m_texture->Destroy();
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void Character::Draw() const {
		m_texture->Render(GetPos(), WidthHeight(), m_screenRatio.Ratio(), true);
#if defined(WITH_DEBUG_VISUALS)
		m_bottomBarDrawable.Render(GetPos(), m_screenRatio.Ratio());
#endif
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void Character::OnRegister (Mover& parMover, Mover::PlaceableTicketType parParentTicket) {
		parMover.RegisterPlaceable(&m_bottomBar, parParentTicket);
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void Character::SetOnBounceCallback (BounceCallbackType parCallb) {
		m_bounceCallback = parCallb;
	}

	///-------------------------------------------------------------------------
	///-------------------------------------------------------------------------
	void Character::OnBounce (const Line<float, 2>& parCollision, const float2& parDirection) {
		if (parDirection.y() < 0.0f) {
			const float2 newPos(GetPos().x(), parCollision.Start().y());
			this->SetAbsolutePosition(newPos);
			m_bottomBar.SetAbsolutePosition(newPos);
			m_bounceCallback(parCollision, parDirection);
		}
	}
} //namespace cloonel
