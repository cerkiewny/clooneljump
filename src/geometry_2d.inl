/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

namespace cloonel {
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T>
	int GetOrientation (const Vector<T, 2>& parPointA, const Vector<T, 2>& parPointB, const Vector<T, 2>& parPointC) {
		const auto ret =
			(parPointB.y() - parPointA.y()) * (parPointC.x() - parPointB.x()) -
			(parPointB.x() - parPointA.x()) * (parPointC.y() - parPointB.y());
		const T zero(0);
		if (ret == zero)
			return 0;
		else if (ret < zero)
			return -1;
		else
			return 1;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T>
	bool Intersection2D (const Vector<T, 2>& parSegA1, const Vector<T, 2>& parSegA2, const Vector<T, 2>& parSegB1, const Vector<T, 2>& parSegB2) {
		const int o1 = GetOrientation(parSegA1, parSegA2, parSegB1);
		const int o2 = GetOrientation(parSegA1, parSegA2, parSegB2);
		const int o3 = GetOrientation(parSegB1, parSegB2, parSegA1);
		const int o4 = GetOrientation(parSegB1, parSegB2, parSegA2);

		if (o1 != o2 and o3 != o4)
			return true;

		if (0 == o1 and IsPointOnSegment(parSegA1, parSegB1, parSegA2))
			return true;
		if (0 == o2 and IsPointOnSegment(parSegA1, parSegB2, parSegA2))
			return true;
		if (0 == o3 and IsPointOnSegment(parSegB1, parSegA1, parSegB2))
			return true;
		if (0 == o4 and IsPointOnSegment(parSegB1, parSegA2, parSegB2))
			return true;

		return false;
	}

	///--------------------------------------------------------------------------
	///http://www.gidforums.com/t-20866.html
	///--------------------------------------------------------------------------
	template <typename T>
	std::pair<bool, Vector<T, 2>> Intersection (const Line<T, 2>& parSegA, const Line<T, 2>& parSegB) {
		typedef Line<T, 2> LineType;

		const typename LineType::Point& startA = parSegA[0];
		const typename LineType::Point& endA = parSegA[1];
		const typename LineType::Point& startB = parSegB[0];
		const typename LineType::Point& endB = parSegB[1];

		const T A1(endA.y() - startA.y());
		const T A2(endB.y() - startB.y());
		const T B1(startA.x() - endA.x());
		const T B2(startB.x() - endB.x());
		const T det(A1 * B2 - A2 * B1);

		if (det == 0) {
			return std::make_pair(false, typename LineType::Point(0));
		}
		else {
			const T C1(A1 * startA.x() + B1 * startA.y());
			const T C2(A2 * startB.x() + B2 * startB.y());
			return std::make_pair(true, typename LineType::Point((B2 * C1 - B1 * C2) / det, (A1 * C2 - A2 * C1) / det));
		}
	}
} //namespace cloonel
