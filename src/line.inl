namespace cloonel {
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Line<T, S>::Line (const Line& parOther) :
		m_points(parOther.m_points)
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Line<T, S>::Line (Scalar parValue) :
		m_points(Point(parValue))
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Line<T, S>::Line (const Point& parStart, const Point& parEnd) :
		m_points(parStart, parEnd)
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Line<T, S>::Line (Scalar parX1, Scalar parY1, Scalar parX2, Scalar parY2) :
		m_points(Point(parX1, parY1), Point(parX2, parY2))
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Line<T, S>& Line<T, S>::operator+= (const Point& parRhs) {
		m_points.x() += parRhs;
		m_points.y() += parRhs;
		return *this;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Line<T, S>& Line<T, S>::operator-= (const Point& parRhs) {
		m_points.x() -= parRhs;
		m_points.y() -= parRhs;
		return *this;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	template <typename U>
	Line<T, S>& Line<T, S>::operator*= (const Vector<U, S>& parRhs) {
		m_points.x() = static_cast<Point>(m_points.x() * parRhs);
		m_points.y() = static_cast<Point>(m_points.y() * parRhs);
		return *this;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Line<T, S> operator+ (const Vector<T, S>& parLhs, Line<T, S> parRhs) {
		parRhs += parLhs;
		return parRhs;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Line<T, S> operator- (const Vector<T, S>& parLhs, Line<T, S> parRhs) {
		parRhs -= parLhs;
		return parRhs;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Line<T, S> operator+ (Line<T, S> parLhs, const Vector<T, S>& parRhs) {
		parLhs += parRhs;
		return parLhs;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	Line<T, S> operator- (Line<T, S> parLhs, const Vector<T, S>& parRhs) {
		parLhs -= parRhs;
		return parLhs;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, typename U, uint32_t S>
	Line<T, S> operator* (const Vector<U, S>& parLhs, Line<T, S> parRhs) {
		parRhs *= parLhs;
		return parRhs;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, typename U, uint32_t S>
	Line<T, S> operator* (Line<T, S> parLhs, const Vector<U, S>& parRhs) {
		parLhs *= parRhs;
		return parLhs;
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T>
	bool operator> (const Vector<T, 2>& parLhs, const Line<T, 2>& parRhs) {
		const Vector<T, 2> line(parRhs.End() - parRhs.Start());
		const Vector<T, 2> linePerp(line.y(), -line.x());
		const Vector<T, 2> pt(parLhs - parRhs.Start());
		const T dotproduct = dot(linePerp, pt);
		return (dotproduct > T(0));
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T>
	bool operator< (const Vector<T, 2>& parLhs, const Line<T, 2>& parRhs) {
		const Vector<T, 2> line(parRhs.End() - parRhs.Start());
		const Vector<T, 2> linePerp(line.y(), -line.x());
		const Vector<T, 2> pt(parLhs - parRhs.Start());
		const T dotproduct = dot(linePerp, pt);
		return (dotproduct < T(0));
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T>
	bool operator>= (const Vector<T, 2>& parLhs, const Line<T, 2>& parRhs) {
		const Vector<T, 2> line(parRhs.End() - parRhs.Start());
		const Vector<T, 2> linePerp(line.y(), -line.x());
		const Vector<T, 2> pt(parLhs - parRhs.Start());
		const T dotproduct = dot(linePerp, pt);
		return (dotproduct >= T(0));
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T>
	bool operator<= (const Vector<T, 2>& parLhs, const Line<T, 2>& parRhs) {
		const Vector<T, 2> line(parRhs.End() - parRhs.Start());
		const Vector<T, 2> linePerp(line.y(), -line.x());
		const Vector<T, 2> pt(parLhs - parRhs.Start());
		const T dotproduct = dot(linePerp, pt);
		return (dotproduct <= T(0));
	}

#if !defined(NDEBUG)
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	template <typename T, uint32_t S>
	std::ostream& operator<< ( std::ostream& parStream, const Line<T, S>& parLine ) {
		for (uint32_t z = 0; z < S - 1; ++z) {
			parStream << parLine[z] << "-";
		}
		parStream << parLine[S - 1];
		return parStream;
	}
#endif
} //namespace cloonel
