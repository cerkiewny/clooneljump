/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CloonelJumpConfig.h"
#include "sdlmain.hpp"
#include "physicsfswrapper.hpp"
#include "gameplaysceneclassic.hpp"
#include "vector.hpp"
#include "casts.hpp"
#include <iostream>
#include <stdexcept>
#include <ciso646>
#include <cstdlib>
#include <ctime>

namespace {
	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void RunMainLoop (cloonel::GameplaySceneClassic& parGame) {
		parGame.Prepare();
		do {
			parGame.Exec();
		} while (not parGame.WantsToQuit());
		parGame.Destroy();
	}
} //unnamed namespace

///------------------------------------------------------------------------------
///following http://twinklebeardev.blogspot.co.uk/2012/07/lesson-1-hello-world.html
///------------------------------------------------------------------------------
int main (int, char* parArgv[]) {
#if defined(NDEBUG)
	std::srand(cloonel::checked_numcast<unsigned int>(std::time(nullptr)));
#else
	std::srand(static_cast<unsigned int>(DEF_RANDOM_SEED));
#endif
	std::cout << GameName << " v" << GameVersionMajor << "." << GameVersionMinor << std::endl;

	int retVal = 0;
	cloonel::SDLMain sdlmain(GameName, cloonel::ushort2(DEF_WIN_WIDTH, DEF_WIN_HEIGHT), cloonel::ushort2(REFERENCE_WIDTH, REFERENCE_HEIGHT));
	try {
		cloonel::PhysicsFSWrapper physfs(parArgv[0]);
		physfs.Append(GAME_BASE_PATH "/resources/", "resources");

		sdlmain.Init();
		std::cout << "Using renderer \"" << sdlmain.GetRendererName() << "\" ";
		std::cout << "and video driver \"" << sdlmain.GetVideoDriverName() << "\"\n";

		cloonel::GameplaySceneClassic game(&sdlmain);
		RunMainLoop(game);
	}
	catch (const std::runtime_error& e) {
		std::cerr << "Error during SDL2 initialization:\n";
		std::cerr << e.what() << std::endl;
		retVal = 1;
	}

	std::cout << "Quitting now" << std::endl;
	return retVal;
}
