/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id56F112C6551D44039D0C0270F573B35B
#define id56F112C6551D44039D0C0270F573B35B

#include "compatibility.h"
#include "vector.hpp"
#include "vectormath.hpp"
#include <ciso646>
#include <cstdint>

namespace cloonel {
	template <typename T, uint32_t S>
	class Line {
	public:
		typedef Vector<T, S> Point;
		typedef T Scalar;

		Line ( void ) {}
		explicit Line ( Scalar parValue );
		Line ( const Line& parOther );
		Line ( const Point& parStart, const Point& parEnd );
		Line ( const Point& parStart, const Point& parDirection, Scalar parLength ) : Line(parStart, parStart + parDirection * parLength) { }
		Line ( Scalar parX1, Scalar parY1, Scalar parX2, Scalar parY2 );
		~Line ( void ) noexcept = default;

		Point& Start ( void ) { return m_points.x(); }
		Point& End ( void ) { return m_points.y(); }
		const Point& Start ( void ) const { return m_points.x(); }
		const Point& End ( void ) const { return m_points.y(); }

		Point& operator[] ( int parIndex ) { assert(parIndex >= 0); return m_points[static_cast<uint32_t>(parIndex)]; }
		const Point& operator[] ( int parIndex ) const { assert(parIndex >= 0); return m_points[static_cast<uint32_t>(parIndex)]; }
		Point& operator[] ( uint32_t parIndex ) { return m_points[parIndex]; }
		const Point& operator[] ( uint32_t parIndex ) const { return m_points[parIndex]; }

		Line& operator+= ( const Point& parRhs ) a_flatten;
		Line& operator-= ( const Point& parRhs ) a_flatten;
		template <typename U>
		Line& operator*= ( const Vector<U, S>& parRhs ) a_flatten;

	private:
		Vector<Point, 2> m_points;
	};

	template <typename T, uint32_t S>
	Line<T, S> operator+ ( Line<T, S> parLhs, const Vector<T, S>& parRhs ) a_pure a_flatten;
	template <typename T, uint32_t S>
	Line<T, S> operator- ( Line<T, S> parLhs, const Vector<T, S>& parRhs ) a_pure a_flatten;
	template <typename T, uint32_t S>
	Line<T, S> operator+ ( const Vector<T, S>& parLhs, Line<T, S> parRhs ) a_pure a_flatten;
	template <typename T, uint32_t S>
	Line<T, S> operator- ( const Vector<T, S>& parLhs, Line<T, S> parRhs ) a_pure a_flatten;
	template <typename T, typename U, uint32_t S>
	Line<T, S> operator* ( const Vector<U, S>& parLhs, Line<T, S> parRhs ) a_pure a_flatten;
	template <typename T, typename U, uint32_t S>
	Line<T, S> operator* ( Line<T, S> parLhs, const Vector<U, S>& parRhs ) a_pure a_flatten;

	template <typename T>
	bool operator> ( const Vector<T, 2>& parLhs, const Line<T, 2>& parRhs ) a_pure;
	template <typename T>
	bool operator< ( const Vector<T, 2>& parLhs, const Line<T, 2>& parRhs ) a_pure;
	template <typename T>
	bool operator>= ( const Vector<T, 2>& parLhs, const Line<T, 2>& parRhs ) a_pure;
	template <typename T>
	bool operator<= ( const Vector<T, 2>& parLhs, const Line<T, 2>& parRhs ) a_pure;
} //namespace cloonel

#include "line.inl"

#endif
