/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id7E6024372BF34999A913A36B6EAB736B
#define id7E6024372BF34999A913A36B6EAB736B

#include "observersmanager.hpp"
#include <memory>
#include <vector>
#include <utility>

namespace cloonel {
	class HorzCollisionBar;
	class CollisionBarSet;

	typedef HorzCollisionBar CollisionBar;

	class Collider {
	public:
		typedef int GroupIDType;

		Collider ( void );
		~Collider ( void ) noexcept;

		void RunCollisionTests ( float parDeltaT ) const;
		void TieGroups ( GroupIDType parGroup1, GroupIDType parGroup2 );
		void RegisterBar ( GroupIDType parGroup, const CollisionBar* parBar );
		void UnregisterBar ( GroupIDType parGroup, const CollisionBar* parBar );
		void RegisterBarSet ( GroupIDType parGroup, const CollisionBarSet* parSet );
		void UnregisterBarSet ( GroupIDType parGroup, const CollisionBarSet* parSet );
		void Reset ( void ) noexcept;

	private:
		typedef std::vector<std::pair<GroupIDType, std::vector<const CollisionBar*>>> CollisionBarGroupListType;
		typedef std::vector<std::pair<GroupIDType, std::vector<const CollisionBarSet*>>> CollisionBarSetsListType;
		typedef std::vector<std::pair<GroupIDType, GroupIDType>> RelationshipListType;

		RelationshipListType m_relationships;
		CollisionBarGroupListType m_collisionBars;
		CollisionBarSetsListType m_collisionBarSets;
	};
} //namespace cloonel

#endif
