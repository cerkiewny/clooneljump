/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "platformspawner.hpp"
#include "platform.hpp"
#include "CloonelJumpConfig.h"
#include "texture.hpp"
#include "gameplayscene.hpp"
#include "mover.hpp"
#include "collider.hpp"
#include "platformset.hpp"
#include "casts.hpp"
#include <ciso646>
#include <cstdlib>
#include <cassert>
#include <algorithm>
#include <boost/noncopyable.hpp>
#include <boost/algorithm/clamp.hpp>
#include <list>

namespace cloonel {
	namespace {
		const uint32_t g_platfWidth = REFERENCE_PLATFORM_WIDTH;
		const uint32_t g_platfHeight = REFERENCE_PLATFORM_HEIGHT;

		float2 PositionForNewPlatf (float parStart, float parAverage, float parMaxDist, size_t parLeft) {
			assert(parAverage >= 0.0f);
			const float& maxDist = parMaxDist;
			const float minDist = std::max(static_cast<float>(g_platfHeight), (static_cast<float>(REFERENCE_HEIGHT) - parStart) / static_cast<float>(parLeft));
			const float average = boost::algorithm::clamp(parAverage, minDist, maxDist);
			assert(minDist <= maxDist); //make sure the player can jump all the way to the top
			assert(minDist >= 0.0f);

			const float upperRange = std::max(average, maxDist) - average;
			const float lowerRange = std::max(average, minDist) - minDist;
			assert(upperRange >= 0.0f);
			assert(lowerRange >= 0.0f);

			const float invRandMax = 1.0f / static_cast<float>(RAND_MAX);
			const float yDist = static_cast<float>(std::rand()) * invRandMax * 2.0f * std::min(lowerRange, upperRange) + minDist;
			assert(yDist >= minDist and yDist <= maxDist);

			const auto rndNum = std::rand() % checked_numcast<int>(REFERENCE_WIDTH - g_platfWidth);

			return float2(static_cast<float>(rndNum), yDist + parStart);
		}
	} //unnamed namespace

	struct PlatformSpawner::LocalData : public boost::noncopyable {
		LocalData ( const char* parTexturePath, SDLMain* parSDLMain, GameplayScene* parScene, float parMaxDistance );
		~LocalData ( void ) noexcept = default;

		PlatformSet platforms;
		Texture texture;
		std::list<std::pair<Collider::GroupIDType, Collider*>> registeredTo;
		GameplayScene* const scene;
		const float maxDistance;
	};

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	PlatformSpawner::LocalData::LocalData (const char* parTexturePath, SDLMain* parSDLMain, GameplayScene* parScene, float parMaxDistance) :
		platforms(parSDLMain),
		texture(parTexturePath, parSDLMain, false),
		scene(parScene),
		maxDistance(parMaxDistance)
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	PlatformSpawner::PlatformSpawner (const char* parTexturePath, SDLMain* parSDLMain, GameplayScene* parScene, float parMaxDistance) :
		Placeable(float2(0.0f)),
		m_localdata(new LocalData(parTexturePath, parSDLMain, parScene, parMaxDistance)),
		m_targetAverage(parMaxDistance / 5.25f) //TODO: change this value to make up for the difficulty
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	PlatformSpawner::~PlatformSpawner() noexcept {
		Destroy();
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSpawner::SpawnPlatforms() {
		const float2 platfWH(static_cast<float>(g_platfWidth), static_cast<float>(g_platfHeight));

		size_t freePlatforms = m_localdata->platforms.CountFreePlatforms();
		float prevPlatf = (m_localdata->platforms.empty() ? 0.0f : m_localdata->platforms.back().GetPos().y() + GetPos().y());
		//FIXME: platform's height shouldn't grow unbounded, they should have the
		//screen-relative position only. Change the movers as appropriate.
		//std::cout << "Top platform is at index " << m_localdata->platforms.size() - 1 << ", prevPlatf = " << prevPlatf << " ";
		//if (not m_localdata->platforms.empty()) {
			//std::cout << "top pos is " << m_localdata->platforms.back().platform->GetPos() << ", system's pos is " << GetPos() << "\n";
		//}
		while (static_cast<float>(REFERENCE_HEIGHT) - prevPlatf >= m_targetAverage) {
			assert(freePlatforms > 0);
			const auto newPos(PositionForNewPlatf(prevPlatf, m_targetAverage, m_localdata->maxDistance, freePlatforms));
			--freePlatforms;
			prevPlatf = newPos.y();

			m_localdata->platforms.Add(newPos, platfWH, &m_localdata->texture);
		}
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSpawner::Prepare() {
		m_localdata->texture.Reload();

		//Spawn the initial platforms
		m_localdata->platforms.clear();
		this->SpawnPlatforms();
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSpawner::Destroy() noexcept {
		m_localdata->texture.Destroy();
		for (const auto& unregPair : m_localdata->registeredTo) {
			unregPair.second->UnregisterBarSet(unregPair.first, &m_localdata->platforms);
		}
		m_localdata->registeredTo.clear();

		m_localdata->platforms.clear();
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSpawner::BeginMovement() {
		m_localdata->platforms.SendBeginMovement();
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSpawner::AddOffset (const float2& parOffset) noexcept {
		m_localdata->platforms.SendAddOffset(parOffset);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSpawner::SetAbsolutePosition (const float2&) noexcept {
		assert(false);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void PlatformSpawner::RegisterForCollision (Collider& parCollider, Collider::GroupIDType parGroupID) {
		parCollider.RegisterBarSet(parGroupID, &m_localdata->platforms);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	const DrawableSet* PlatformSpawner::GetDrawableSet() const {
		return &m_localdata->platforms;
	}
} //namespace cloonel
