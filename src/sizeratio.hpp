/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id3098F08C14B84E3C8CE169CBA05C9C86
#define id3098F08C14B84E3C8CE169CBA05C9C86

#include "vector.hpp"

namespace cloonel {
	class SizeRatio {
	public:
		SizeRatio ( void ) = default;
		explicit SizeRatio ( const float2& parOriginal );
		SizeRatio ( const float2& parOriginal, const float2& parSize );
		~SizeRatio ( void ) noexcept = default;

		const float2& Ratio ( void ) const noexcept { return m_ratio; }
		const float2& Resolution ( void ) const noexcept { return m_size; }
		void SetOriginal ( const float2& parOriginal, const float2& parRes );
		void UpdateResolution ( const float2& parNewRes );

	private:
		float2 m_original;
		float2 m_size;
		float2 m_ratio;
	};
} //namespace cloonel

#endif
