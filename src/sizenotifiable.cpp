#include "sizenotifiable.hpp"
#include "sizeratio.hpp"
#include "sdlmain.hpp"
#include <cassert>
#include <ciso646>

namespace cloonel {
	namespace implem {
	} //namespace implem

	namespace regbehaviours {
		///----------------------------------------------------------------------
		///----------------------------------------------------------------------
		void AutoRegister::Register (SizeNotifiableBase* parNotifiable) {
			assert(m_sdlmain);
#if !defined(NDEBUG)
			assert(not m_registered);
			m_registered = true;
#endif
			m_id = m_sdlmain->RegisterForResChange(parNotifiable);
		}

		///----------------------------------------------------------------------
		///----------------------------------------------------------------------
		void AutoRegister::Unregister() noexcept {
#if !defined(NDEBUG)
			assert(m_registered or not m_sdlmain);
			m_registered = false;
#endif
			if (m_sdlmain) {
				m_sdlmain->UnregisterForResChange(m_id);
			}
		}

		///----------------------------------------------------------------------
		///----------------------------------------------------------------------
		AutoRegister::AutoRegister (AutoRegister&& parOther, SizeNotifiableBase* parSwapTo) :
			m_sdlmain(nullptr),
			m_id(0)
#if !defined(NDEBUG)
			, m_registered(false)
#endif
		{
			std::swap(m_sdlmain, parOther.m_sdlmain);
			std::swap(m_id, parOther.m_id);
#if !defined(NDEBUG)
			std::swap(m_registered, parOther.m_registered);
#endif
			assert(m_sdlmain);
			m_sdlmain->SwapRegisteredForResChange(m_id, parSwapTo);
		}
	} //namespace regbehaviours

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void SizeNotifiableBase::NotifyResChanged (const SizeRatio& parSize) {
		m_scaleRatio = parSize.Ratio();
	}
} //namespace cloonel
