/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef id0F37904CB7274575B7E9419E615DA250
#define id0F37904CB7274575B7E9419E615DA250

#include "vector.hpp"
#include <string>

struct SDL_Texture;

namespace cloonel {
	class SDLMain;

	class Texture {
	public:
		Texture ( const std::string& parPath, SDLMain* parMain, bool parLoadNow );
		~Texture ( void ) noexcept;

		void Reload ( void );
		void Destroy ( void ) noexcept;
		bool IsLoaded ( void ) const { return nullptr != m_texture; }
		void Render ( const float2& parPos, const float2& parScaling, bool parClip ) const { Render(parPos, m_size, parScaling, parClip); }
		void Render ( const float2& parPos, const float2& parSize, const float2& parScaling, bool parClip ) const;
		const SDLMain* SDLObject ( void ) const { return m_sdlmain; }

	private:
		const std::string m_path;
		float2 m_size;
		SDL_Texture* m_texture;
		SDLMain* const m_sdlmain;
	};
} //namespace cloonel

#endif
