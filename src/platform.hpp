/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idDBDFC83B5EBA417D96A342111BFA463E
#define idDBDFC83B5EBA417D96A342111BFA463E

#include "sizenotifiable.hpp"
#include "drawable.hpp"
#include "placeable.hpp"
#include "collidertypedef.hpp"
#include <memory>
#include <vector>

namespace cloonel {
	class Texture;
	class SDLMain;
	class HorzCollisionBar;

	class Platform : public Drawable, public Placeable {
	public:
		enum {
			SurfaceCount = 1
		};

		Platform ( SDLMain* parSdlMain, const float2& parPos, Texture* parTexture, const float2& parSize );
		Platform ( Platform&& parOther ) noexcept;
		Platform ( const Platform& ) = delete;
		virtual ~Platform ( void ) noexcept;
		Platform& operator= ( const Platform& parOther );

		float2 TopLeft ( void ) const { return GetPos(); }
		float2 BottomRight ( void ) const { return TopLeft() + m_size; }
		const HorzCollisionBar* TopCollisionBar ( void ) const { return m_collisionTop.get(); }
		void CopyDrawables ( std::vector<const Drawable*>& parOut ) const;

		//Overrides
		virtual void Draw ( void ) const;
		virtual void OnRegister ( Mover& parMover, Mover::PlaceableTicketType parParentTicket );

	private:
		SizeNotifiable<regbehaviours::AutoRegister> m_screenRatio;
		float2 m_size;
		ColliderRegisterFunc m_registerToCollider;
		ColliderUnregisterFunc m_unregisterFromCollider;
		std::unique_ptr<HorzCollisionBar> m_collisionTop;
		Texture* m_surface;
	};
} //namespace cloonel

#endif
