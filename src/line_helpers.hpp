/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idDF0B3D1FA4714EF3AB1241C3DA0D4E3D
#define idDF0B3D1FA4714EF3AB1241C3DA0D4E3D

#include "vector.hpp"
#include "line.hpp"
#include "compatibility.h"
#include <cmath>

namespace cloonel {
	template <typename T, uint32_t S>
	T len ( const Line<T, S>& parLine ) a_pure;

	template <typename T, uint32_t S>
	T len_sq ( const Line<T, S>& parLine ) a_pure;
} //namespace cloonel

#include "line_helpers.inl"

#endif
