/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef id1DA767EB516A4F4588347DFC14D1A999
#define id1DA767EB516A4F4588347DFC14D1A999

#if defined(WITH_DEBUG_VISUALS)
#include <cstdint>

namespace cloonel {
	struct Colour {
		typedef uint8_t ChannelType;

		Colour ( void ) = default;
		Colour ( ChannelType parR, ChannelType parG, ChannelType parB, ChannelType parA );
		Colour ( ChannelType parR, ChannelType parG, ChannelType parB ) : Colour(parR, parG, parB, 0xFF) {}
		explicit Colour ( ChannelType parFill );
		Colour ( const Colour& parOther ) = default;

		ChannelType r, g, b, a;
	};

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	inline Colour::Colour (ChannelType parR, ChannelType parG, ChannelType parB, ChannelType parA) :
		r(parR),
		g(parG),
		b(parB),
		a(parA)
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	inline Colour::Colour (ChannelType parFill) :
		r(parFill),
		g(parFill),
		b(parFill),
		a(parFill)
	{
	}
} //namespace cloonel

#endif
#endif
