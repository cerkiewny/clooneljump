#include "sizeratio.hpp"
#include "compatibility.h"

namespace cloonel {
	namespace {
		float2 CalculateRatio ( float2 parOriginal, float2 parResolution ) a_pure;

		///----------------------------------------------------------------------
		///----------------------------------------------------------------------
		float2 CalculateRatio (float2 parOriginal, float2 parResolution) {
			return parResolution / parOriginal;
		}
	} //unnamed namespace

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	SizeRatio::SizeRatio (const float2& parOriginal) :
		m_original(parOriginal),
		m_size(parOriginal),
		m_ratio(1.0f)
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	SizeRatio::SizeRatio (const float2& parOriginal, const float2& parSize) :
		m_original(parOriginal),
		m_size(parSize),
		m_ratio(CalculateRatio(parOriginal, parSize))
	{
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void SizeRatio::SetOriginal (const float2& parOriginal, const float2& parRes) {
		m_original = parOriginal;
		m_size = parRes;
		m_ratio = CalculateRatio(parOriginal, parRes);
	}

	///--------------------------------------------------------------------------
	///--------------------------------------------------------------------------
	void SizeRatio::UpdateResolution (const float2& parNewRes) {
		m_size = parNewRes;
		m_ratio = CalculateRatio(m_original, parNewRes);
	}
} //namespace cloonel
