/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idid0528646832E04CF08E9785B66CFE0BD1
#define idid0528646832E04CF08E9785B66CFE0BD1

#include "compatibility.h"
#include <cstdint>
#include <ciso646>
#include <type_traits>
#include <algorithm>
#include <cassert>

#if !defined(NDEBUG)
#include <iostream>
#endif

#if defined(__INTEL_COMPILER)
#	define DONT_GUESS_NOEXCEPT
#endif

namespace cloonel {
	template <typename T, uint32_t S>
	class Vector {
		template <typename U, uint32_t R> friend class Vector;
	public:
#if defined(DONT_GUESS_NOEXCEPT)
		Vector ( void ) = default;
		explicit Vector ( T parValue );
		template <typename U> explicit Vector ( const Vector<U, S>& parOther );
#else
		Vector ( void ) noexcept(noexcept(T())) = default;
		explicit Vector ( T parValue ) noexcept(noexcept(T()) && noexcept(parValue=parValue));
		template <typename U> explicit Vector ( const Vector<U, S>& parOther ) noexcept(noexcept(T()) && noexcept(const_cast<U&>(parOther.m_mem[0])=T()));
#endif
		template <typename = std::enable_if<S == 2> > Vector ( T parX, T parY ) noexcept : m_mem {parX, parY} {}
		template <typename = std::enable_if<S == 3> > Vector ( T parX, T parY, T parZ ) noexcept : m_mem {parX, parY, parZ} {}
		template <typename = std::enable_if<S == 4> > Vector ( T parX, T parY, T parZ, T parW ) noexcept : m_mem {parX, parY, parZ, parW} {}

		~Vector ( void ) noexcept = default;

		enum {
			Dimension = S
		};

		template <typename = std::enable_if<           4 >= S> > T& x ( void ) { return m_mem[0]; }
		template <typename = std::enable_if<           4 >= S> > const T& x ( void ) const { return m_mem[0]; }
		template <typename = std::enable_if<S >= 2 and 4 >= S> > T& y ( void ) { return m_mem[1]; }
		template <typename = std::enable_if<S >= 2 and 4 >= S> > const T& y ( void ) const { return m_mem[1]; }
		template <typename = std::enable_if<S >= 3 and 4 >= S> > T& z ( void ) { return m_mem[2]; }
		template <typename = std::enable_if<S >= 3 and 4 >= S> > const T& z ( void ) const { return m_mem[2]; }
		template <typename = std::enable_if<S >= 4 and 4 >= S> > T& w ( void ) { return m_mem[3]; }
		template <typename = std::enable_if<S >= 4 and 4 >= S> > const T& w ( void ) const { return m_mem[3]; }

		template <typename U> const Vector& operator+= ( const Vector<U, S>& parOther );
		template <typename U> const Vector& operator-= ( const Vector<U, S>& parOther );
		template <typename U> const Vector& operator*= ( const Vector<U, S>& parOther );
		template <typename U> const Vector& operator/= ( const Vector<U, S>& parOther );
		template <typename U> const Vector& operator+= ( U parOther );
		template <typename U> const Vector& operator-= ( U parOther );
		template <typename U> const Vector& operator*= ( U parOther );
		template <typename U> const Vector& operator/= ( U parOther );

		T& operator[] ( uint32_t parIndex ) { assert(parIndex < S); return m_mem[parIndex]; }
		const T& operator[] ( uint32_t parIndex ) const { assert(parIndex < S); return m_mem[parIndex]; }

	private:
		T m_mem[S];
	};

	template <typename T, typename U, uint32_t S>
	Vector<typename std::common_type<T, U>::type, S> operator+ ( const Vector<T, S>& parA, const Vector<U, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S>
	Vector<typename std::common_type<T, U>::type, S> operator- ( const Vector<T, S>& parA, const Vector<U, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S>
	Vector<typename std::common_type<T, U>::type, S> operator* ( const Vector<T, S>& parA, const Vector<U, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S>
	Vector<typename std::common_type<T, U>::type, S> operator/ ( const Vector<T, S>& parA, const Vector<U, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S, typename=typename std::enable_if<std::is_fundamental<U>::value>::type>
	Vector<typename std::common_type<T, U>::type, S> operator+ ( U parA, const Vector<T, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S, typename=typename std::enable_if<std::is_fundamental<U>::value>::type>
	Vector<typename std::common_type<T, U>::type, S> operator- ( U parA, const Vector<T, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S, typename=typename std::enable_if<std::is_fundamental<U>::value>::type>
	Vector<typename std::common_type<T, U>::type, S> operator* ( U parA, const Vector<T, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S, typename=typename std::enable_if<std::is_fundamental<U>::value>::type>
	Vector<typename std::common_type<T, U>::type, S> operator/ ( U parA, const Vector<T, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S, typename=typename std::enable_if<std::is_fundamental<U>::value>::type>
	Vector<typename std::common_type<T, U>::type, S> operator+ ( const Vector<T, S>& parA, U parB ) a_pure;
	template <typename T, typename U, uint32_t S, typename=typename std::enable_if<std::is_fundamental<U>::value>::type>
	Vector<typename std::common_type<T, U>::type, S> operator- ( const Vector<T, S>& parA, U parB ) a_pure;
	template <typename T, typename U, uint32_t S, typename=typename std::enable_if<std::is_fundamental<U>::value>::type>
	Vector<typename std::common_type<T, U>::type, S> operator* ( const Vector<T, S>& parA, U parB ) a_pure;
	template <typename T, typename U, uint32_t S, typename=typename std::enable_if<std::is_fundamental<U>::value>::type>
	Vector<typename std::common_type<T, U>::type, S> operator/ ( const Vector<T, S>& parA, U parB ) a_pure;

	template <typename T, typename U, uint32_t S>
	bool operator< ( const Vector<T, S>& parA, const Vector<U, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S>
	bool operator> ( const Vector<T, S>& parA, const Vector<U, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S>
	bool operator<= ( const Vector<T, S>& parA, const Vector<U, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S>
	bool operator>= ( const Vector<T, S>& parA, const Vector<U, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S>
	bool operator== ( const Vector<T, S>& parA, const Vector<U, S>& parB ) a_pure;
	template <typename T, typename U, uint32_t S>
	bool operator!= ( const Vector<T, S>& parA, const Vector<U, S>& parB ) a_pure;

	template <typename T, uint32_t S>
	Vector<T, S> operator- ( Vector<T, S> parOperand ) a_pure;

	typedef Vector<float, 2> float2;
	typedef Vector<uint16_t, 2> ushort2;
#if !defined(NDEBUG)
	typedef Vector<int16_t, 2> short2;
#endif
	typedef Vector<int32_t, 2> int2;

#if !defined(NDEBUG)
	template <typename T, uint32_t S>
	std::ostream& operator<< ( std::ostream& parStream, const Vector<T, S>& parVector ) {
		parStream << "<";
		for (uint32_t z = 0; z < S - 1; ++z) {
			parStream << parVector[z] << ",";
		}
		parStream << parVector[S - 1] << ">";
		return parStream;
	}
#endif

	namespace implem {
		template <typename T, typename U>
		struct CategorizeTypes {
			typedef typename std::common_type<T, U>::type CommonType;
			typedef typename std::conditional<std::is_same<CommonType, T>::value, U, T>::type OtherType;
		};
		template <typename Cat, uint32_t S, bool Straightforward=std::is_same<typename Cat::CommonType, typename Cat::OtherType>::value>
		struct DoOperation {
			static Vector<typename Cat::CommonType, S> do_mul ( Vector<typename Cat::CommonType, S> parLho, const Vector<typename Cat::CommonType, S>& parRho ) {
				parLho *= parRho;
				return parLho;
			}
			static Vector<typename Cat::CommonType, S> do_div ( Vector<typename Cat::CommonType, S> parLho, const Vector<typename Cat::CommonType, S>& parRho ) {
				parLho /= parRho;
				return parLho;
			}
			static Vector<typename Cat::CommonType, S> do_sum ( Vector<typename Cat::CommonType, S> parLho, const Vector<typename Cat::CommonType, S>& parRho ) {
				parLho += parRho;
				return parLho;
			}
			static Vector<typename Cat::CommonType, S> do_sub ( Vector<typename Cat::CommonType, S> parLho, const Vector<typename Cat::CommonType, S>& parRho ) {
				parLho -= parRho;
				return parLho;
			}
		};
		template <typename Cat, uint32_t S>
		struct DoOperation<Cat, S, false> {
			static Vector<typename Cat::CommonType, S> do_mul ( Vector<typename Cat::CommonType, S> parLho, const Vector<typename Cat::OtherType, S>& parRho ) {
				parLho *= parRho;
				return parLho;
			}
			static Vector<typename Cat::CommonType, S> do_mul ( const Vector<typename Cat::OtherType, S>& parLho, const Vector<typename Cat::CommonType, S>& parRho ) {
				Vector<typename Cat::CommonType, S> ret(parLho);
				ret *= parRho;
				return ret;
			}
			static Vector<typename Cat::CommonType, S> do_div ( Vector<typename Cat::CommonType, S> parLho, const Vector<typename Cat::OtherType, S>& parRho ) {
				parLho /= parRho;
				return parLho;
			}
			static Vector<typename Cat::CommonType, S> do_div ( const Vector<typename Cat::OtherType, S>& parLho, const Vector<typename Cat::CommonType, S>& parRho ) {
				Vector<typename Cat::CommonType, S> ret(parLho);
				ret /= parRho;
				return ret;
			}
			static Vector<typename Cat::CommonType, S> do_sum ( Vector<typename Cat::CommonType, S> parLho, const Vector<typename Cat::OtherType, S>& parRho ) {
				parLho += parRho;
				return parLho;
			}
			static Vector<typename Cat::CommonType, S> do_sum ( const Vector<typename Cat::OtherType, S>& parLho, const Vector<typename Cat::CommonType, S>& parRho ) {
				Vector<typename Cat::CommonType, S> ret(parLho);
				ret += parRho;
				return ret;
			}
			static Vector<typename Cat::CommonType, S> do_sub ( Vector<typename Cat::CommonType, S> parLho, const Vector<typename Cat::OtherType, S>& parRho ) {
				parLho -= parRho;
				return parLho;
			}
			static Vector<typename Cat::CommonType, S> do_sub ( const Vector<typename Cat::OtherType, S>& parLho, const Vector<typename Cat::CommonType, S>& parRho ) {
				Vector<typename Cat::CommonType, S> ret(parLho);
				ret -= parRho;
				return ret;
			}
		};
	} //namespace implem
} //namespace cloonel
#include "vector.inl"

#if defined(DONT_GUESS_NOEXCEPT)
#undef DONT_GUESS_NOEXCEPT
#endif

#endif
