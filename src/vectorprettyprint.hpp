/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idE31BEAEB229149038D67C6CF370AEEC9
#define idE31BEAEB229149038D67C6CF370AEEC9

#include "vector.hpp"

#if !defined(NDEBUG)
#include <iostream>
#endif

namespace cloonel {
#if !defined(NDEBUG)
	template <typename T, uint32_t S>
	inline std::ostream& operator<< (std::ostream& parOStream, const Vector<T, S>& parVec) {
		parOStream << "<";
		for (uint32_t z = 0; z < S - 1; ++z) {
			parOStream << parVec[z] << ", ";
		}
		parOStream << parVec[S - 1] << ">";
		return parOStream;
	}

#endif
} //namespace cloonel

#endif
