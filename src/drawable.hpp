/*
	Copyright 2014 Michele "King_DuckZ" Santullo

	This file is part of CloonelJump.

	CloonelJump is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	CloonelJump is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with CloonelJump.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef idC5A880D06A03407DB4E9FC21593A47FB
#define idC5A880D06A03407DB4E9FC21593A47FB

#include "vector.hpp"

namespace cloonel {
	class SDLMain;

	class Drawable {
	public:
		Drawable ( void ) = default;
		Drawable ( float parWidth, float parHeight );
		explicit Drawable ( float2 parWH );
		virtual ~Drawable ( void ) noexcept;

		virtual void Draw ( void ) const = 0;

	protected:
		const float2& WidthHeight ( void ) const { return m_wh; }

	private:
		float2 m_wh;
	};
} //namespace cloonel

#endif
